# PyTorch Template Example

This repository provides a example of [PyTorch Template](https://gitlab.com/raniere-phd/pytorch-template)
using MNIST data.

## Requirements

- [Conda](https://docs.conda.io/)
- [Comet](https://www.comet.ml/) account

## Setting Environment

### Conda

Run

```
$ conda env create --file environment.yml
```

to install all Python dependencies.

### Comet

Run

```
$ comet init --api-key
```

to create a Comet configuration file with your API key.

### Data

Run

```
$ wget https://github.com/myleott/mnist_png/raw/master/mnist_png.tar.gz
$ tar xvzf mnist_png.tar.gz
```

## Machine Learning Experiment

Run

```
$ python main.py
```

Metrics will be save in Comet.

![Screenshot of Comet](./img/comet.png)

## Files and Folders

- `agent.py`

  Has the knowledge to handle the data and model for train, validation and test.
- `data`

  Store the data. It should be replace with a symbolic link to the data.
- `dataset.py`

  Has the knowledge to load the data and create batches.
- `environment.yml`

  Conda environment.
- `main.py`

  Runnable script. It stores some hyperparameters.
- `model.py`

  Has the implementation of the model using PyTorch.
- `weights`

  Store the weights of trained model.

## Related Projects

- https://github.com/moemen95/PyTorch-Project-Template