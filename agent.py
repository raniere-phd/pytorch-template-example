"""
Agent

Includes all the functions that train, validate, and test the model.
"""
import torch
from torch import nn
import torch.optim as optim

import dataset
import model


class Agent:
    """
    This base class will contain the base functions to be overloaded by any agent you will implement.
    """

    def __init__(self, hyper_params, logger=None):
        """Create instance of Agent"""
        self.hyper_params = hyper_params
        self.logger = logger
        self.model = model.AlexNet().to(self.hyper_params['device'])
        self.data_train_loader = None
        self.data_test_loader = None

    def train(self):
        """
        Main training loop
        :return:
        """
        if self.data_train_loader is None:
            self.data_train_loader = dataset.DataLoader(
                train=True,
                self.hyper_params['batch_size'],
                shuffle=True
            )

        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.SGD(
            self.model.parameters(), lr=0.001, momentum=0.9)

        for epoch in range(self.hyper_params['num_epochs']):
            running_loss = self.train_one_epoch()
            self.logger.log_metric(
                "epooch_loss",
                running_loss / len(self.data_train_loader),
                step=epoch
            )

    def train_one_epoch(self):
        """
        One epoch of training
        :return:
        """
        running_loss = 0

        for datum in self.data_train_loader:
            inputs, labels = datum.values()
            inputs = inputs.to(self.hyper_params['device'], dtype=torch.float)
            labels = labels.to(self.hyper_params['device'], dtype=torch.long)

            # zero the parameter gradients
            self.optimizer.zero_grad()

            # forward + backward + optimize
            outputs = self.model(inputs)
            loss = self.criterion(outputs, labels)
            loss.backward()
            self.optimizer.step()

            running_loss += loss.item()

        return running_loss

    def validate(self):
        """
        Main validate loop
        :return:
        """
        pass

    def test(self):
        """
        Main test loop
        :return:
        """
        correct = 0

        if self.data_test_loader is None:
            self.data_test_loader = dataset.DataLoader(
                train=True,
                self.hyper_params['batch_size'],
                shuffle=False
            )

        for datum in self.data_test_loader:
            inputs, labels = datum.values()
            inputs = inputs.to(self.hyper_params['device'], dtype=torch.float)
            labels = labels.to(self.hyper_params['device'], dtype=torch.long)

            outputs = self.model(inputs)

            correct += (outputs == labels).sum()

        self.logger.log_metric(
            'correct',
            correct
        )

    def finalize(self):
        """
        Finalizes all the operations
        :return:
        """
        pass
