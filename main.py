"""
Main

Manager the agent to train, validate, and test the model.
At the end of the operation,
a report is submited to Comet.
"""
import comet_ml
import torch

from agent import Agent

hyper_params = {
    'device': 'cuda' if torch.cuda.is_available() else 'cpu',
    'num_epochs': 2,
    'batch_size': 64,
}


def main():
    """Main loop"""
    experiment = comet_ml.Experiment(
        project_name="pytorch-template"
    )
    experiment.log_parameters(hyper_params)

    agent = Agent(
        hyper_params,
        logger=experiment
    )

    with experiment.train():
        agent.train()

    with experiment.validate():
        pass

    with experiment.test():
        agent.test()

    agent.finalize()


if __name__ == '__main__':
    main()
