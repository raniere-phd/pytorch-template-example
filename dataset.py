"""
Dataset

Includes all the functions that either preprocess or postprocess data.
"""
import os
import os.path

import pandas as pd
import torch.utils.data
from torchvision.io import read_image
import torchvision.transforms


class Dataset(torch.utils.data.Dataset):
    """MNIST dataset"""

    def __init__(self, train=True):
        """Create instance of MNIST dataset"""
        if train:
            self.data_path = 'data/mnist_png/training'
        else:
            self.data_path = 'data/mnist_png/testing'

        df = pd.DataFrame(columns=["filename", "label"])

        for folder in os.listdir(self.data_path):
            sub_folder = os.path.join(self.data_path, folder)
            file_lists = [f for f in os.listdir(
                sub_folder) if os.path.isfile(os.path.join(sub_folder, f))]
            labels = list(folder) * len(file_lists)
            c = {
                "filename": file_lists,
                "label": labels
            }
            data = pd.DataFrame(c)
            df = pd.concat(
                [df, data],
                ignore_index=True
            )

        self.df = df.astype({
            'label': 'int32',  # Need cast the column so that PyTorch will do implicit type conversion
        })

    def __len__(self):
        return self.df.shape[0]

    def __getitem__(self, idx):
        img_path = os.path.join(
            self.data_path,
            str(self.df.loc[idx, 'label']),
            self.df.loc[idx, 'filename']
        )
        image = torchvision.transforms.functional.resize(
            read_image(img_path),
            (225, 225)
        )

        label = self.df.loc[idx, 'label']

        sample = {"image": image, "label": label}

        return sample

    def plot_samples_per_epoch(self):
        """
        Plotting the batch images
        :return: img_epoch: which will contain the image of this epoch
        """
        raise NotImplementedError

    def make_gif(self):
        """
        Make a gif from a multiple images of epochs
        :return:
        """
        raise NotImplementedError

    def finalize(self):
        pass


class DataLoader(torch.utils.data.DataLoader):
    """MNIST data loader"""

    def __init__(self, batch_size, train=True, shuffle=True):
        """Create data loader instance."""
        super().__init__(
            Dataset(train=train),
            batch_size,
            shuffle=shuffle
        )
